# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] 2020-07-15

### Added
Introduced two new environment variables: `gain` and `integration-time` to allow the sensor settings to be configured through the script.
Added a check to handle sensor overflow errors when reading lux.

## [1.0.0] 2020-07-14

### Added

Initial release of the reader for the TSL2591 sensor. Submits light (lux), visible light (no unit) and infrared light (no unit) to the station-monitoring-service.

[1.0.1]: https://gitlab.com/residential-climate-monitoring/sensors/tsl2591/-/releases/rcm-tsl2591-reader-1.0.1
[1.0.0]: https://gitlab.com/residential-climate-monitoring/sensors/tsl2591/-/releases/rcm-tsl2591-reader-1.0.0
